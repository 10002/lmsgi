<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output encoding="utf-8" indent="yes" method="html" doctype-system="about:legacy-compat" />
	<xsl:template match="/">
		<html>
			<head>
				<title></title>
				<style>
					th { background-color: lightgray; }
					img { width: 128px; }
				</style>
			</head>
			<body>
				<table border="1">
					<tr>
						<th>Título</th><th>Director</th><th>Carátula</th>
					</tr>
					<xsl:for-each select="peliculas/pelicula">
						<tr>
							<td><xsl:value-of select="titulo"/></td>
							<td><xsl:value-of select="director"/></td>
							<td><img src="{foto}"/></td>
						</tr>
					</xsl:for-each>
				</table>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>