<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output encoding="utf-8" indent="yes" method="html" doctype-system="about:legacy-compat" />
	<xsl:template match="/">
		<html>
			<head>
				<title></title>
			</head>
			<body>
				<table border="1">
					<tr bgcolor="lightgray">
						<th>Título</th><th>País</th><th>Productor</th><th>Director</th><th>Año</th>
						<th>Duración</th><th>Género</th><th>Sinopsis</th><th>URL</th>
					</tr>
					<xsl:for-each select="peliculas/pelicula">
						<tr>
							<td><xsl:value-of select="titulo"/></td>
							<td><xsl:value-of select="nacionalidad"/></td>
							<td><xsl:value-of select="productor"/></td>
							<td><xsl:value-of select="director"/></td>
							<td><xsl:value-of select="anio"/></td>
							<td><xsl:value-of select="duracion"/></td>
							<td><xsl:value-of select="genero"/></td>
							<td><xsl:value-of select="sinopsis"/></td>
							<td><xsl:value-of select="url"/></td>
						</tr>
					</xsl:for-each>
				</table>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>