<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output encoding="utf-8" indent="yes" method="html" doctype-system="about:legacy-compat" />
	<xsl:template match="/">
		<html>
			<head>
				<title>Listado de CDs de música</title>
				<meta charset="utf-8"/>
				<style>
					.conBorde { border: 1px solid black; }
					th { background-color: lightgray; }
					.canciones { width: 100%; }
				</style>
			</head>
			<body>
			<h1>Listado de CDs de música</h1>
				<table>
					<tr>
						<th>Título</th><th>Artista</th><th>Discográfica</th><th>Año</th><th>Canciones</th>
					</tr>
					<xsl:for-each select="cds/cd">
						<tr>
							<td class="conBorde"><xsl:value-of select="@titulo"/></td>
							<td class="conBorde"><xsl:value-of select="@artista"/></td>
							<td class="conBorde"><xsl:value-of select="@discografica"/></td>
							<td class="conBorde"><xsl:value-of select="@año"/></td>
							<td class="conBorde">
								<table>
									<tr>
										<th>Duración</th><th class="canciones">Título</th>
									</tr>
									<xsl:for-each select="canciones/cancion">
										<tr>
											<td><xsl:value-of select="@duracion"></xsl:value-of></td>
											<td><xsl:value-of select="."></xsl:value-of></td>
										</tr>														
									</xsl:for-each>
								</table>
							</td>
						</tr>
					</xsl:for-each>
				</table>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>