<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output encoding="utf-8" indent="yes" method="html" doctype-system="about:legacy-compat" />
	<xsl:template match="/">
		<html>
			<head>
				<title>Listado de canciones del sello discográfico Geffen Records</title>
				<meta charset="utf-8"/>
				<style>
					.conBorde { border: 1px solid black; }
					th { background-color: lightgray; }
				</style>
			</head>
			<body>
				<table class="conBorde">
					<tr>
						<th>Título</th><th>Duración</th>
					</tr>
					<xsl:for-each select="cds/cd[@discografica='Geffen Records']/canciones/cancion">
						<tr>
							<td><xsl:value-of select="."/></td>
							<td><xsl:value-of select="@duracion"/></td>
						</tr>
					</xsl:for-each>
				</table>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>